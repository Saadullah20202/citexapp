import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {Category} from '../pages/category/category';
import{Questions} from '../pages/questions/questions';
import {Finish} from '../pages/finish/finish';
import {HttpModule} from '@angular/http';
import { DragulaModule } from 'ng2-dragula';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import {Emailtutor} from '../pages/emailtutor/emailtutor';
import {About} from '../pages/about/about';
import { Contact } from '../pages/contact/contact';
import { Thankemail } from '../pages/thankemail/thankemail';






@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Category,
    Questions,
    Finish,
    Emailtutor,
    About,
    Contact,
    Thankemail
  
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    DragulaModule,
    RoundProgressModule
    ],
    schemas:[CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Category,
    Questions,
    Finish,
    Emailtutor,
    About,
    Contact,
    Thankemail
  ],
  providers: [
    StatusBar,
    SplashScreen,

    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
