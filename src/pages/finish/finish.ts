import { Component } from '@angular/core';
import { NavController,NavParams, PopoverController } from 'ionic-angular';
import { HomePage } from '../home/home';

import { RoundProgressConfig} from 'angular-svg-round-progressbar';
import {Emailtutor} from '../emailtutor/emailtutor';
import { Contact } from '../contact/contact';
import { About } from '../about/about';



/**
 * Generated class for the Finish page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-finish',
  templateUrl: 'finish.html',
})
export class Finish {
  correct:any;
  wrong:any;
  total:any;
  grandtotal:any;
  popup_menu:any;
  popupclose:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController,
    ) {

     
    this.popup_menu = true;
    this.correct = navParams.data.correct;
    this.wrong = navParams.data.wrong;
    this.total = navParams.data.total;

    console.log('correct '+this.correct);
    console.log('wrong '+this.wrong);
    console.log('total '+this.total);

    this.grandtotal = (this.correct/this.total)*100;
    console.log(this.grandtotal);

    


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Finish');
  }

  restartTest(){
        this.navCtrl.setRoot(HomePage);
  }


  popover(){
    if(!this.popupclose){
      this.popup_menu = false;
      this.popupclose = true;
      console.log('open');
    }else{
      this.popupclose = false;
      this.popup_menu = true;
      console.log('false');
    }
}
  

  emailtotutor(){
    this.navCtrl.push(Emailtutor,{
        correct : this.correct,
        wrong : this.wrong,
        total:this.total,
        grandtotal: this.grandtotal
    })
  }

  contact(){
    
    this.popupclose = false;
    this.popup_menu = true;
    this.navCtrl.push(Contact);
  }
  
  about(){
    this.popup_menu = true;
    this.popupclose = false;
    this.navCtrl.push(About);
  }
}
