import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { Contact } from '../contact/contact';


/**
 * Generated class for the About page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class About {
  popup_menu:any;
  popupclose:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.popup_menu = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad About');
  }

  popover(){
    if(!this.popupclose){
      this.popup_menu = false;
      this.popupclose = true;
      console.log('open');
    }else{
      this.popupclose = false;
      this.popup_menu = true;
      console.log('false');
    }
    
}

  contact(){
    
    this.popupclose = false;
    this.popup_menu = true;
    this.navCtrl.push(Contact);
  }
  
  about(){
    this.popup_menu = true;
    this.popupclose = false;
    this.navCtrl.push(About);
  }

}
