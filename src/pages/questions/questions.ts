import { Component } from '@angular/core';
import { NavController,NavParams,  PopoverController  } from 'ionic-angular';
import {Http ,Headers} from '@angular/http';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { Finish } from '../finish/finish';
import * as $ from 'jquery';
import { Contact } from '../contact/contact';
import { About } from '../about/about';



 
/**
 * Generated class for the Questions page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-questions',
  templateUrl: 'questions.html',
  providers: [DragulaService]
})
export class Questions {

    quiztype:any;
    select_style:any;
    id:any;
    url:any = 'http://citex.org.uk/citex-api.php?method=fetch_data';
    getquestions:any;
    questions:any;
    loader:any;
    questionarray:any;
    question_text:any;
    errormsg:any;
    questionmsg:any;
    error_text:any;
    repeatables:any;
    answers_array:any;
    confusing_array:any;
    answers_new_array:any;
    fixed:any;
    repeatdata:any = [];
    count:number =1;
    total:any;
    previous:any;
    nextbutton:any;
    repeats_data:any;

    beforeDropRepeatables: any;
    index:number = 0;
    fixtures:any;
    questionmsg1:any;
    temp:any = [];
    testing:any;
    value:number = 0;
    incorrect:any;
    correct:any;
    checkanswers:any;
    correctanswers:number = 0;
    wronganswers:number = 0;

    drag_id:any;
    drag_value:any;
    pushvalue:any;
    fill_id:any;
    popup_menu:any;
    popupclose:boolean = false;
    dragval:any;
    help_popup:any;


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public http:Http,
              public dragulaService:DragulaService,
              public popoverCtrl: PopoverController) {
            this.popup_menu = true;
            this.nextbutton = false;
            this.errormsg = true;
            this.previous = true;
            this.questionmsg = true;
            this.loader = false;
            this.quiztype = navParams.data.quiztype;
            this.select_style = navParams.data.select_style;
            this.id = navParams.data.id;
            this.questionarray = [];
            this.answers_new_array = [];
            this.questionmsg1 = true;
            this.confusing_array = [];
            this.repeatables = [];
            this.questions = [];  
            this.beforeDropRepeatables = [];  
            this.testing = [];
            this.incorrect = true;
            this.correct = true;
            this.checkanswers = [];
            this.help_popup = true;
          
          
            this.dragulaService.setOptions('any-bag',{
                copy: function(el, source) {
                  return "copy-drop" == source.id ;
                },
               accepts: function(el, target, source, sibling) {
                 return "drag-element" == target.className ;
               },
              
            });

                      dragulaService.drop.subscribe(value => { 
                          this.onDrop(value.slice(1));
                        });

                      dragulaService.drag.subscribe(value => {
                          this.onDrag(value.slice(1));
                        });

                        dragulaService.over.subscribe((value) => {                          
                          this.onOver(value.slice(1));
                        });


            this.getquestions = {
              question_type: this.quiztype,
              question_class: this.select_style,
              subcategory: this.id
            }

            this.getquestions = JSON.stringify(this.getquestions);

            let head = new Headers();
            head.append('Content-Type','application/x-www-form-urlencoded');

            this.http.post(this.url, this.getquestions , {headers:head})
            .subscribe(data => {
                console.log(data.json());
              this.repeats_data = data.json().repeatables;
                this.loader = true;
                if(data.json().error != ''){
                  this.error_text = data.json().error;
                  this.errormsg = false;
                  this.questionmsg = true;
                }else{
                this.questions = data.json().questions;
                  for(let j= 0; j<this.questions.length; j++){
                    this.questions[j] = Object.assign(this.questions[j],{repeats:this.repeats_data})
                  }
                this.total = this.questions.length;
                this.errormsg = true;
                this.questionmsg = false; 
                for(let i=0; i<this.questions.length;i++){
                    this.checkanswers.push('?');
                    this.questionarray[i] = this.questions[i];
                    this.callingindedxquestion(this.index);
                }
                }  
            });
  }

 
  //////////////////////////// ON DROP FUNCTION

  onDrop(args) {
    
   
    
    /////////////////// Delete element from confusing array ////////////////////

    if(args[1]) {
      var drop_id = args[1].id; 
        
      for(let i=0;i<this.confusing_array.length;i++){
                  if(this.drag_value == this.confusing_array[i]){
                                this.confusing_array.splice(i,1);
                  }
                }

          $('div#'+drop_id+'.drag-element div').remove();
          var elem = ' <div id='+this.drag_id+' >'+this.drag_value+'</div>';
          $('div#'+drop_id+'.drag-element').append(elem);
          $('div#'+drop_id).css({"border-bottom":"px solid #ccc"});
    }


    ///////////////////////////// when element drop element and if testarray is not empty then it can push element in confusing array ////////////////
          if(args[1] != null){
                  
                var id = args[1].id.split('_');
                var index_drop = id[1];
                //console.log('id is '+index_drop);



                if(this.testing[index_drop] != '' || this.testing[index_drop] == this.drag_value){
                  if(this.testing[index_drop] != "," &&
                     this.testing[index_drop] != "." &&
                     this.testing[index_drop] != ";" &&
                     this.testing[index_drop] != ":" &&
                     this.testing[index_drop] != "{" &&
                     this.testing[index_drop] != "}" &&
                     this.testing[index_drop] != "(" &&
                     this.testing[index_drop] != ")" &&
                     this.testing[index_drop] != "!" &&
                     this.testing[index_drop] != "<" &&
                     this.testing[index_drop] != ">" ){
                    this.confusing_array.push(this.testing[index_drop]);
                  }
                  
                }

        
                if(this.fill_id != undefined){
                  this.testing[this.fill_id] = "";
                }
                
                this.testing[index_drop] = args[0].innerText;
          
          }
  }


//////////////////////////// END ON DROP FUNCTION

 
//////////////////////////// ON DRAD FUNCTION
  private onDrag(args) {

    
    this.drag_id = args[0].id;
    /// Drag element id
    this.dragval = this.drag_id.split('_')[2];

    //drop element id 
    this.fill_id = args[1].id.split('_')[1];

    this.drag_value = args[0].innerText;
    $( ".blank-row" ).each(function( index ) {
      $( this ).find(".drag-element").attr('id','drop_'+index);
    });
  }


  ///////////////////////////////////////ON DRAG END

  ///////////////////////////////////////ON OVER START
  private onOver(args) {
    let [e, el, container] = args;
  }
////////////////////////////ON OVER END

     
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad Questions');
  }

  ////////////////////Previous question call

  previousquestion(){

    this.correct = true;
    this.incorrect = true;
   

    $('.dropable-button .drag-element div').remove();
        
        this.count = this.count - 1;
        this.index = this.index - 1;
        this.nextbutton = false;
        if(this.count == 1){
          this.previous = true;
        }
         this.callingindedxquestion(this.index);

  }


  //////////////////////////////// Reset Question call
  reset(){

      this.correct = true;
      this.incorrect = true;
    
      $('.dropable-button .drag-element div').remove();
  
      this.callingindedxquestion(this.index);

      console.log('reset');

  }


  /////////////////////////////////// Check question call
  check(alreadycheck){

        console.log(this.testing);
        var flag = 0;
          if(this.answers_array.length == this.testing.length){
            for(let i=0; i<this.answers_array.length;i++){
                if(this.answers_array[i] == this.testing[i]){
                  flag = flag + 1;
                } 
          }
          console.log(flag)
          if(flag == this.answers_array.length){
            this.checkanswers[alreadycheck] = 1;
            this.correct = false;
            this.incorrect = true;
          }else{
            this.checkanswers[alreadycheck] = 0;
            this.correct = true;
            this.incorrect = false;
          }
        console.log('check');
  }
}


////////////////////////////////////////Finish Call
  finish(){
          console.log(this.checkanswers);
          for(let f=0;f<this.checkanswers.length;f++){
            if(this.checkanswers[f] == 1){
              this.correctanswers += 1;
            }
          else{  
              this.wronganswers += 1;
            }
          }

        this.navCtrl.setRoot(Finish, {
          correct : this.correctanswers,
          wrong : this.wronganswers,
          total : this.total
        });
  }


//////////////////////////////////////// Next Call
  next(){

          this.correct = true;
          this.incorrect = true;
          
          $('.dropable-button .drag-element div').remove();
        

          this.previous = false;
          this.count = this.count + 1;
          this.index = this.index + 1;
          if(this.count == this.total){
            this.nextbutton = true;
          }
          
          if(this.index < this.total){
            this.callingindedxquestion(this.index);
            
            }
    }


    /////////////////////////////////// All Qustion call from there 

    callingindedxquestion(index){
          this.testing = [];
          this.confusing_array = []
          this.question_text = this.questionarray[index].question_text;
          this.answers_array = this.questionarray[index].answer;
          this.temp = this.questionarray[index].confusing;
          this.fixed = this.questionarray[index].fixed;
          this.repeatables = ["<",">",",","(",")",".","{","}",":",";","!"];
          let diff = this.answers_array
          .filter((x) => this.repeatables.indexOf(x) == -1)
          .filter((x) => this.temp.indexOf(x) == -1);
          this.confusing_array = this.temp.concat(diff);
          this.confusing_array = this.confusing_array; //this.shuffle(this.confusing_array);
          for(let j=0; j<this.fixed.length;j++){
            if(this.fixed[j] == ''){
              this.testing.push("");
            }
       }
    }


    //////////////////////////////////////// Shuffle Array elements

    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
    
      return array;
    }
    
//////////////////////////////////////// PopOver call fri=om menu button
    popover(){
      if(!this.popupclose){
        this.popup_menu = false;
        this.popupclose = true;
        console.log('open');
      }else{
        this.popupclose = false;
        this.popup_menu = true;
        console.log('false');
      }
    }

    close(){
      this.help_popup = true;
    }
    openpop(){
      this.help_popup = false;
    }

    contact(){
      
      this.popupclose = false;
      this.popup_menu = true;
      this.navCtrl.push(Contact);
    }
    
    about(){
      this.popup_menu = true;
      this.popupclose = false;
      this.navCtrl.push(About);
    }
  }


