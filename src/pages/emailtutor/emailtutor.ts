import { Component } from '@angular/core';
import { NavController,NavParams, PopoverController,ToastController } from 'ionic-angular';
import {Http ,Headers} from '@angular/http';
import { Contact } from '../contact/contact';
import { About } from '../about/about';
import { Thankemail } from '../thankemail/thankemail';

/**
 * Generated class for the Emailtutor page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-emailtutor',
  templateUrl: 'emailtutor.html',
})
export class Emailtutor {

  correct:any;
  wrong:any;
  total:any;
  grandtotal:any;
  studentname;
  email;
  comment;
  url:any = 'http://citex.org.uk/citex-api.php?method=send_email';
  loader:any;
  popup_menu:any;
  popupclose:boolean = false;
  select_style :any = localStorage.getItem('select_style').toUpperCase();
  quiz_type :any = localStorage.getItem('quiz_type');
  category:any = localStorage.getItem('category');
  book_reference:any = this.category + ', ' + this.quiz_type;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public popoverCtrl: PopoverController,public http: Http,
              public toastCtrl:ToastController) {
                this.loader = true;
                this.popup_menu = true;
                console.log(this.select_style);
                this.correct = navParams.data.correct;
                this.wrong = navParams.data.wrong;
                this.total = navParams.data.total;
                this.grandtotal = navParams.data.grandtotal;
                
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Emailtutor');
  }

  popover(){
    if(!this.popupclose){
      this.popup_menu = false;
      this.popupclose = true;
      console.log('open');
    }else{
      this.popupclose = false;
      this.popup_menu = true;
      console.log('false');
    }
}

  sendemail(){
   
  this.loader = false;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


if((this.studentname != null || this.studentname != '') && (this.email != null || this.email != '') && re.test(this.email)){
  var senddata = {
    student_name:this.studentname,
    tutor_email:this.email,
    comment:this.comment,
    correct:this.correct,
    total:this.total,
    wrong:this.wrong,
    grand_total: this.grandtotal+'%',
    style:this.select_style,
    book_reference : this.book_reference
  }

  var senddatapost = JSON.stringify(senddata);

  let head = new Headers();
  head.append('Content-Type','application/x-www-form-urlencoded');

  this.http.post(this.url, senddatapost, {headers:head})
  .subscribe(data => {
        this.loader = true;
        this.studentname = "";
        this.email = "";
        this.comment = "";
        localStorage.removeItem('quiz_type');
        localStorage.removeItem('select_style');
        localStorage.removeItem('category');
        this.navCtrl.setRoot(Thankemail);


  },err => {
    this.loader = true;
    this.presentToast('Invalid Data');
  })

}else{
  this.loader = true;
    this.presentToast('Please Check your Name and tutors Email');
}

      

  }


   /* ---------------------------- Toast Function ---------------------------------------*/
   private presentToast(text) {
    let toast = this.toastCtrl.create({
          message: text,
          duration: 3000,
          position: 'top'
     });
toast.present();
}
/* ----------------------------END Toast Function ---------------------------------------*/


contact(){
  
  this.popupclose = false;
  this.popup_menu = true;
  this.navCtrl.push(Contact);
}

about(){
  this.popup_menu = true;
  this.popupclose = false;
  this.navCtrl.push(About);
}
}
