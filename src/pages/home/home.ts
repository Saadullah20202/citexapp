import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from '@angular/http';
import {Category} from '../category/category';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html',

})
export class HomePage {

  select_style:any = 'harvard';
  quiztype:any;
  reference_cat:any;
  citation_cats:any
  loader:any;
  
  constructor(public navCtrl: NavController,
              public http:Http
            ) {

              this.loader = false;
              localStorage.removeItem('quiz_type');
              localStorage.removeItem('select_style');
              localStorage.removeItem('category');

           http.get('http://citex.org.uk/citex-api.php?method=init_data').subscribe(
             data => {
                this.reference_cat = data.json().reference_cats;
                this.citation_cats = data.json().citation_cats;
                this.loader = true;
             }
           );
               
              

            
  }

  refrencelist(){

    var quiz_type = 'Reference List';
    this.quiztype = 'reference';
    localStorage.setItem('quiz_type',quiz_type);
    localStorage.setItem('select_style',this.select_style);
    this.navCtrl.push(Category,{
      select_style:this.select_style,
      quiztype: this.quiztype,
      category : this.reference_cat
    });
  
  }

  textcitation(){
    var quiz_type = 'In-text citation';
    this.quiztype = 'syntax';
    localStorage.setItem('quiz_type',quiz_type);
    localStorage.setItem('select_style',this.select_style);
     this.navCtrl.push(Category,{
      select_style:this.select_style,
      quiztype: this.quiztype,
      category : this.citation_cats
    });
    
  }


}
