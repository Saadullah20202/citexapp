import { Component } from '@angular/core';
import { NavController,NavParams, Platform  } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the Thankemail page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-thankemail',
  templateUrl: 'thankemail.html',
})
export class Thankemail {
  popup_menu:any;
  popupclose:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public platform:Platform) {
    this.popup_menu = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Thankemail');
  }

  restartTest(){
    this.navCtrl.setRoot(HomePage);
  }

  exit(){
    console.log('hello')
    this.platform.exitApp();
  }

  popover(){
    if(!this.popupclose){
      this.popup_menu = false;
      this.popupclose = true;
      console.log('open');
    }else{
      this.popupclose = false;
      this.popup_menu = true;
      console.log('false');
    }
  }
}
