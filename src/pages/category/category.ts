import { Component } from '@angular/core';
import { NavController,NavParams, PopoverController } from 'ionic-angular';
import {Questions} from '../questions/questions';
import { Contact } from '../contact/contact';
import { About } from '../about/about';



/**
 * Generated class for the Category page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class Category {

  category:any;
  category1:any = [];
  quiztype:any;
  select_style:any;
  popup_menu:any;
  popupclose:boolean = false;


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public popoverCtrl: PopoverController
              ) {
                this.popup_menu = true;
            this.category = navParams.data.category;
            this.quiztype = navParams.data.quiztype;
            this.select_style = navParams.data.select_style;

          Object.keys(this.category).map((key) => {
            this.category1.push(Object.assign({}, {id: key}, this.category[key]));
          });

         

  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad Category');
  }

  question(id,title){
    console.log(title);
    localStorage.setItem('category', title);
    this.navCtrl.push(Questions, {
        id:id,
        quiztype:this.quiztype,
        select_style:this.select_style
    });

  }

  popover(){
    if(!this.popupclose){
      this.popup_menu = false;
      this.popupclose = true;
      console.log('open');
    }else{
      this.popupclose = false;
      this.popup_menu = true;
      console.log('false');
    }

    
}

  contact(){
    
    this.popupclose = false;
    this.popup_menu = true;
    this.navCtrl.push(Contact);
  }
  
  about(){
    this.popup_menu = true;
    this.popupclose = false;
    this.navCtrl.push(About);
  }
}
