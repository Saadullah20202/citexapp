<?php

header("Access-Control-Allow-Origin: *");
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

// GET REQUESTS
if($_SERVER['REQUEST_METHOD'] === 'GET') {
    // citex-api.php?method=init_data
    if(isset($_REQUEST['method']) && $_GET['method'] == 'init_data') {
        return wp_send_json(initData());
    }
}

// POST REQUESTS
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $post_data = file_get_contents("php://input");
    $params = json_decode($post_data, true);

    // citex-api.php?method=fetch_data
    if(isset($_REQUEST['method']) && $_REQUEST['method'] == 'fetch_data') {
        $response = fetchData($params);
        return wp_send_json($response);
    }

    // citex-api.php?method=send_email
    if(isset($_REQUEST['method']) && $_REQUEST['method'] == 'send_email') {
        $response = sendEmail($params);
        return wp_send_json($response);
    }
}

return wp_send_json(array(
    'message' => 'API called with incorrect parameters.',
), 500);

function initData() {
    $reference_cats = array();
    $reference_terms = get_terms(array(
        'taxonomy' => 'reference-categories',
        'hide_empty' => false,
    ));
    $reference_icons = array(
        16 => 'fa-book',
        17 => 'fa-book',
        18 => 'fa-newspaper-o',
        19 => 'fa-globe'
    );
    foreach ($reference_terms as $r) {
        $id = $r->term_id;
        $reference_cats[$id]['title'] = $r->name;
        $reference_cats[$id]['description'] = $r->description;
        $reference_cats[$id]['icon'] = isset($reference_icons[$id]) ? $reference_icons[$id] : 'fa-list';
    }

    $citation_terms = get_terms(array(
        'taxonomy' => 'citation-categories',
        'hide_empty' => false,
    ));
    $citation_icon = array(
        12 => 'fa-user',
        13 => 'fa-users',
        14 => 'fa-users',
        15 => 'fa-users'
    );
    $citation_cats = array();
    foreach ($citation_terms as $r) {
        $id = $r->term_id;
        $citation_cats[$id]['title'] = $r->name;
        $citation_cats[$id]['description'] = $r->description;
        $citation_cats[$id]['icon'] = isset($citation_icon[$id]) ? $citation_icon[$id] : 'fa-list';
    }

    $output = array();

    $output['reference_cats'] = $reference_cats;
    $output['citation_cats'] = $citation_cats;
    return $output;
}

function fetchData($params) {

    if (isset($params['question_type']) && isset($params['question_class']) && isset($params['subcategory'])) {
        $type = $params['question_type'];
        $subcat = $params['subcategory'];
        $class = $params['question_class'];

        if ($type == 'reference') {
            $post_type = 'citex-reference';
            $taxonomy = 'reference-categories';
        } else {
            $post_type = 'citex-citations';
            $taxonomy = 'citation-categories';
        }

        $loop = new WP_Query(array(
            'post_type' => $post_type,
            'posts_per_page' => 20,
            'meta_key'      => 'question_class',
            'meta_value'    => $class,
            'tax_query' => array(
                array(
                    'taxonomy' => $taxonomy,
                    'field' => 'term_id',
                    'terms' => $subcat,
                )
            ),
            'orderby' => 'rand'
        ));

        $questions = array();
        $output = array();
        $repeats = array();
        if ( $loop->have_posts() ) {
            while ($loop->have_posts()) {
                $loop->the_post();
                array_push($questions, getQuestion(get_the_ID()));
            }
            wp_reset_postdata();

            // repeatables
            $loop = new WP_Query(array(
                'post_type' => 'repeatable-parts',
                'posts_per_page' => -1,
                'orderby' => 'rand'
            ));
            if ($loop->have_posts()) {
                while ($loop->have_posts()) {
                    $loop->the_post();
                    array_push($repeats, get_field('character_word', get_the_ID()));
                }
            }

            $response = array();
            $response['error'] = '';
            $response['questions'] = $questions;
            $response['repeatables'] = $repeats;
            return $response;
        } else {
            $response = array();
            $response['error'] = 'No questions found in selected category/sub category. please change category or question type.';
            return $response;
        }
    } else {
        $response = array();
        $response['error'] = 'Please select category';
        return $response;
    }
}

function sendEmail($params) {
    if(
        !isset($params['student_name']) ||
        !isset($params['tutor_email']) ||
        !isset($params['correct']) ||
        !isset($params['wrong']) ||
        !isset($params['total']) ||
        !isset($params['grand_total']) ||
		!isset($params['style']) ||
		!isset($params['book_reference']) ||
        !filter_var($params['tutor_email'], FILTER_VALIDATE_EMAIL)
    ) {
        $response = array();
        $response['error'] = 'Data missing.';
        return $response;
    }

    $to = $params['tutor_email'];
    $subject = 'Citex Test';
	
	$body = '<b>Name</b>: ' . $params['student_name'] .'<br/>';
	$body .= '<b>Style</b>: ' . $params['style'] .'<br/>';
	$body .= '<b>Book Reference</b>: ' . $params['book_reference'] .'<br/>';
    $body .= '<b>Correct</b>: ' . $params['correct'] .'<br/>';
    $body .= '<b>Wrong</b>: ' . $params['wrong'] .'<br/>';
    $body .= '<b>Total</b>: ' . $params['total'] .'<br/>';
    $body .= '<b>Grand Total</b>: ' . $params['grand_total'] .'<br/>';
    if(isset($params['comments'])) {
        $body .= '<b>Comments</b>: ' . $params['comments'] .'<br/>';
    }

    $headers = array('Content-Type: text/html; charset=UTF-8');
    $response = array();
    $response['success'] = wp_mail( $to, $subject, $body, $headers );;
    return $response;
}

function getQuestion($id) {
    $final = array();
    $question = array();
    $confusion = array();
    $has_repeatables='no';

    if (have_rows('question_parts', $id)) {
        while (have_rows('question_parts', $id)) {
            the_row();
            if (get_sub_field('is_repeatable') == 'yes') {
                $has_repeatables='yes';
                array_push($question, get_sub_field('repeatable'));
            } else {
                array_push($question, get_sub_field('parts'));
            }
        }
    }

    if (have_rows('confusing_words', $id)) {
        while (have_rows('confusing_words', $id)) {
            the_row();
            array_push($confusion, get_sub_field('words'));
        }
    }

    $final['answer'] = $question;
    $final['has_repeatables'] = $has_repeatables;
    $final['question_text'] = get_field('question_text',$id);
    $final['confusing'] = $confusion;

    if ( get_post_type($id) == 'citex-citations' ) {
        $final['no_fills'] = 'No';
        if (get_field('fixed_text', $id) != '') {
            $fixed_parts = explode('|', get_field('fixed_text', $id));
        }
        $final['fixed'] = $fixed_parts;
    } else {
        $final['no_fills'] = 'No';
        if (get_field('fixed_text', $id) != '') {
            $fixed_parts = explode('|', get_field('fixed_text', $id));
        }
        $final['fixed'] = $fixed_parts;
    }
    return $final;
}
?>
